#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
//template <typename pizza>
struct Node;

//template <typename pizza>
struct Krawedz {
	int dane;
	Node *poczatek;
	Node *koniec;
	Krawedz();
	Krawedz(Node *jeden, Node *dwa);
	Krawedz(int cos, Node *jeden, Node *dwa);
};

//template <typename pizza>
Krawedz::Krawedz() {
	dane = 0;
	poczatek = nullptr;
	koniec = nullptr;
};

Krawedz::Krawedz(Node *jeden, Node *dwa) {
	dane = 0;
	poczatek = jeden;
	koniec = dwa;
};

//template <typename pizza>
Krawedz::Krawedz(int cos, Node *jeden, Node *dwa) {
	dane = cos;
	poczatek = jeden;
	koniec = dwa;
};

//template <typename pizza>
struct Node {
	int dane;
	vector<Krawedz*> lista_krawedzi;
	Node();
	Node(int cos);
};

//template <typename pizza>
Node::Node() {
	dane = 0;
	//tu_jestem = new Node;
};

//template <typename pizza>
Node::Node(int cos) {
	dane = cos;
	//tu_jestem = COS;
};

//template <typename pizza>
struct Graf {
	vector<Node*> lista_wezlow;
	vector<Krawedz*> lista_krawedzi;
	Node **endVertices(int cos);
	Node *opposite(int cos, int meaning_of_life);
	bool Sasiedzi(int Kargul, int Pawlak);
	void replace(int v, int x);
	void replace_kraw(int v, int x);
	void insert_Node(int cos);
	void insert_Kraw(int cos, int pol, int lop);
	void usun_krawedz(Krawedz *cos);
	void usun(int cos);
	void pokaz_swoje_krawedzie(int v);
	void pokaz_wszystkie_krawedzie();
	void pokaz_wszystkie_wezly();
};

Node** Graf::endVertices(int cos)
{
	int i = 0;
	Node** tab = new Node*[2];
	tab[0] = new Node();
	tab[1] = new Node();
	while (i<lista_krawedzi.size() && cos != lista_krawedzi[i]->dane)
	{
		i++;
	}
	if (i < lista_krawedzi.size())
	{
		tab[0] = lista_krawedzi[i]->poczatek;
		tab[1] = lista_krawedzi[i]->koniec;
		return tab;
	}
	else
	{
		cout << "Brak szukanej krawedzi." << endl;
		tab[0]->dane = 0;
		tab[1]->dane = 0;
		return tab;
	}
}

Node* Graf::opposite(int wezel, int cos)
{
	Node *elemencik = new Node;
	elemencik = lista_wezlow[0];
	//Node *zakoncz = new Node;
	//zakoncz = *lista_wezlow->end();
	int i = 1;
	cout << lista_wezlow.size() << endl;
	while (elemencik->dane != wezel && i < lista_wezlow.size())
	{
		elemencik = lista_wezlow[i];
		i++;
	cout << "POMOC" << endl;
	}
	Krawedz *elemencior = new Krawedz;
	elemencior = elemencik->lista_krawedzi[0];
	cout<< elemencior->dane<<endl;
	while (elemencior->dane != cos)
		elemencior++;
	if (wezel == elemencior->poczatek->dane)
		return elemencior->koniec;
	else if (wezel == elemencior->koniec->dane)
		return elemencior->poczatek;
	else
	{
		cout << "Cos poszlo nie tak." << endl;
		return elemencik;
	}
}

bool Graf::Sasiedzi(int Kargul, int Pawlak)
{
	Krawedz* element = lista_krawedzi[0];
	while (element != nullptr)
	{
		if (Kargul == element->poczatek->dane)
		{
			if (Pawlak == element->koniec->dane)
			{
				return true;
			}
		}
		else if (Kargul == element->koniec->dane)
		{
			if (Pawlak == element->poczatek->dane)
			{
				return true;
			}
		}
		else
			element++;
	}
	return false;
}

void Graf::replace(int v, int x)
{
	int i = 0;
	while (v != lista_wezlow[i]->dane)
		i++;
	lista_wezlow[i]->dane = x;
}

void Graf::replace_kraw(int v, int x)
{
	int i = 0;
	while (v != lista_krawedzi[i]->dane)
		i++;
	lista_krawedzi[i]->dane = x;
}

void Graf::insert_Node(int cos)
{
	Node *cos1 = new Node(cos);
	lista_wezlow.push_back(cos1);
}

void Graf::insert_Kraw(int cos, int pol, int lop)
{
	int i = 0;
	Krawedz *krawedz = new Krawedz();
	krawedz->dane = cos;
	while (i<lista_wezlow.size() && pol != lista_wezlow[i]->dane)
	{
		i++;
	}
	if (i < lista_wezlow.size())
	{
		krawedz->poczatek = lista_wezlow[i];
	}
	else
	{
		insert_Node(pol);
		krawedz->poczatek = lista_wezlow[i];
	}
	lista_wezlow[i]->lista_krawedzi.push_back(krawedz);
	i = 0;
	while (i < lista_wezlow.size() && lop != lista_wezlow[i]->dane)
	{
		i++;
		//cout << "Pomoc" << endl;
	}
	if (i < lista_wezlow.size())
	{
		krawedz->koniec = lista_wezlow[i];
	}
	else
	{
		insert_Node(lop);
		krawedz->koniec = lista_wezlow[i];
	}
	lista_wezlow[i]->lista_krawedzi.push_back(krawedz);
	lista_krawedzi.push_back(krawedz);
}

void Graf::usun_krawedz(Krawedz *cos)
{
	int i = 0;
	while (i<cos->poczatek->lista_krawedzi.size() && cos != cos->poczatek->lista_krawedzi[i])
		i++;
	if (i < cos->poczatek->lista_krawedzi.size())
	{
		cos->poczatek->lista_krawedzi.erase(cos->poczatek->lista_krawedzi.begin() + i);
		i = 0;
		while (cos != cos->koniec->lista_krawedzi[i])
			i++;
		cos->koniec->lista_krawedzi.erase(cos->koniec->lista_krawedzi.begin() + i);
		i = 0;
		while (cos != lista_krawedzi[i])
			i++;
		lista_krawedzi.erase(lista_krawedzi.begin() + i);
		delete cos;
	}
	else
		cout << "Brak podanej krawedzi." << endl;
}

void Graf::usun(int cos)
{
	int j = 0;
	while (j < lista_wezlow.size() && cos != lista_wezlow[j]->dane)
		j++;
	if (j < lista_wezlow.size())
	{
		Node *jak = lista_wezlow[j];
		int i = 0;
		while (i < jak->lista_krawedzi.size())
		{
			usun_krawedz(jak->lista_krawedzi[i]);
			i++;
		}
		lista_wezlow.erase(lista_wezlow.begin() + i);
		delete jak;
	}
	else
		cout << "Brak poszukiwanego wezla." << endl;
}

void Graf::pokaz_swoje_krawedzie(int v)
{
	int i = 0;
	vector<Krawedz*> kop;
	while (i < lista_wezlow.size() && v != lista_wezlow[i]->dane)
		i++;
	if (i < lista_wezlow.size())
	{
		kop = lista_wezlow[i]->lista_krawedzi;
		for (int j = 0; j < kop.size(); j++)
		{
			cout << kop[j]->dane << endl;
		}
	}
	else
		cout << "Brak szukanego wezla." << endl;
}

void Graf::pokaz_wszystkie_krawedzie()
{
	vector<Krawedz*> kop;
	kop = lista_krawedzi;
	if (kop.size()>0)
	{
		for (int i = 0; i < kop.size(); i++)
			cout << kop[i]->dane << endl;
	}
	else
	{
		cout << "Brak krawedzi." << endl;
	}
}

void Graf::pokaz_wszystkie_wezly()
{
	vector<Node*> kop;
	kop = lista_wezlow;
	if (kop.size()>0)
	{
		for (int i = 0; i < kop.size(); i++)
			cout << kop[i]->dane << endl;
	}
	else
	{
		cout << "Brak wezlow." << endl;
	}
}

int main()
{
	Graf *Grafik = new Graf;
	Node *cos1 = new Node(12);
	Node *cos2 = new Node(2);
	Krawedz *kraw = new Krawedz(5, cos1, cos2);
	cos1->lista_krawedzi.push_back(kraw);
	cos2->lista_krawedzi.push_back(kraw);
	//cos1->lista_krawedzi->push_back(kraw);
	Grafik->lista_wezlow.push_back(cos2);
	Grafik->lista_wezlow.push_back(cos1);
	//kraw->dane = 5;
	//kraw->koniec = cos2;
	//kraw->poczatek = cos1;
	Grafik->lista_krawedzi.push_back(kraw);
	//cout << "COS" << endl;
	int kop = Grafik->opposite(2, 5)->dane;
	cout << kop << endl;
	cout << "Krawedzie sasiadujace do 5: " << endl;
	Node **tab = Grafik->endVertices(5);
	cout << tab[0]->dane << endl;
	cout << tab[1]->dane << endl;
	cout << "Zastapiono element 2 przez 7." << endl;
	Grafik->replace(2,7);
	cout << "Zastapiono krawedz 5 przez 9." << endl;
	Grafik->replace_kraw(5,9);
	cout << "Krawedzie sasiadujace do 9: " << endl;
	tab = Grafik->endVertices(9);
	cout << tab[0]->dane << endl;
	cout << tab[1]->dane << endl;
	cout << "Dodano krawedz 6 laczaca wezly 7 i 8." << endl;
	Grafik->insert_Kraw(6, 7, 8);
	cout << "Krawedzie sasiadujace do 6: " << endl;
	tab = Grafik->endVertices(6);
	cout << tab[0]->dane << endl;
	cout << tab[1]->dane << endl;
	//cout<< Grafik->lista_krawedzi[1]->dane << endl;
	//Grafik->usun(7);
	//cout << Grafik->lista_krawedzi.size() << endl;
	cout << "Usunieto krawedz 1." << endl;
	Grafik->usun_krawedz(Grafik->lista_krawedzi[1]);
	tab = Grafik->endVertices(6);
	cout << tab[0]->dane << endl;
	cout << tab[1]->dane << endl;
	cout << "Oto krawedzie 7." << endl;
	Grafik->pokaz_swoje_krawedzie(7);
	cout << "Oto wszystkie wezly grafu." << endl;
	Grafik->pokaz_wszystkie_wezly();
	return 0;
}