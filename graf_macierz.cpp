#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
//template <typename pizza>
struct Node;

//template <typename pizza>
struct Krawedz {
	int dane;
	Node *poczatek;
	Node *koniec;
	Krawedz();
	Krawedz(Node *jeden, Node *dwa);
	Krawedz(int cos, Node *jeden, Node *dwa);
};

//template <typename pizza>
Krawedz::Krawedz() {
	dane = 0;
	poczatek = nullptr;
	koniec = nullptr;
};

Krawedz::Krawedz(Node *jeden, Node *dwa) {
	dane = 0;
	poczatek = jeden;
	koniec = dwa;
};

//template <typename pizza>
Krawedz::Krawedz(int cos, Node *jeden, Node *dwa) {
	dane = cos;
	poczatek = jeden;
	koniec = dwa;
};

//template <typename pizza>
struct Node {
	int dane;
	int indeks;
	Node();
	Node(int cos);
};

//template <typename pizza>
Node::Node() {
	dane = 0;
	//tu_jestem = new Node;
};

//template <typename pizza>
Node::Node(int cos) {
	dane = cos;
	//tu_jestem = COS;
};

//template <typename pizza>
struct Graf {
	vector<Node*> lista_wezlow;
	Krawedz* **matrix;
	int rozmiar;
	int ilosc;
	int indeks_wezla(int cos);
	Node **endVertices(Krawedz* cos);
	Node *opposite(int cos, int meaning_of_life);
	bool Sasiedzi(int Kargul, int Pawlak);
	void replace(int v, int x);
	void powieksz();
	void insert_Node(int cos);
	void insert_Kraw(int cos, int pol, int lop);
	void usun_krawedz(int cos);
	void usun(int cos);
	void pokaz_swoje_krawedzie(int v);
	void pokaz_wszystkie_krawedzie();
	void pokaz_wszystkie_wezly();
	Graf();
};

Graf::Graf()
{
	rozmiar = 5;
	matrix = new Krawedz* *[rozmiar];
	for (int i = 0; i < rozmiar; i++)
	{
		matrix[i] = new Krawedz*[rozmiar];
		for (int j = 0; j < rozmiar; j++)
		{
			matrix[i][j] = nullptr;
		}
	}
	ilosc = 0;
};

Node** Graf::endVertices(Krawedz* cos)
{
	Node **tab = new Node*[2];
	if (cos != nullptr)
	{
		cout << "Krawedz o wadze " << cos->dane << " laczy wierzcholki " << cos->poczatek->dane << " i " << cos->koniec->dane << "." << endl;
		tab[0] = cos->poczatek;
		tab[1] = cos->koniec;
	}
	else
	{
		cout << "Niestety szukana krawedz nie istnieje." << endl;
		tab[0]->dane = 0;
		tab[1]->dane = 0;
	}
	return tab;
}

Node* Graf::opposite(int wezel, int cos)
{
	Node *pomoc = new Node;
	if (indeks_wezla(wezel) != -1)
	{
		int i = indeks_wezla(wezel);
		for (int j = 0; j < lista_wezlow.size(); j++)
		{
			if (matrix[i][j] != nullptr)
			{
				if (matrix[i][j]->dane == cos)
				{
					if (matrix[i][j]->poczatek->dane == wezel)
					{
						pomoc = matrix[i][j]->koniec;
						return pomoc;
					}
					else
					{
						pomoc = matrix[i][j]->poczatek;
						return pomoc;
					}
				}
			}
		}
		cout << endl;
	}
	else
	{
		cout << "Brak szukanego wezla." << endl;
	}
	return pomoc;
}

bool Graf::Sasiedzi(int Kargul, int Pawlak)
{
	if (matrix[indeks_wezla(Kargul)][indeks_wezla(Pawlak)]!=nullptr)
	{
		cout << "Sa to sasiedzi." << endl;
		return true;
	}
	else if (matrix[indeks_wezla(Kargul)][indeks_wezla(Pawlak)] != nullptr)
	{
		cout << "Sa to sasiedzi." << endl;
		return true;
	}
	else
	{
		cout << "Nie sa sasiadami." << endl;
		return false;
	}
}

void Graf::replace(int v, int x)
{
	if (indeks_wezla(v) == -1)
	{
		cout << "Brak szukanego wezla." << endl;
	}
	else
	{
		lista_wezlow[indeks_wezla(v)]->dane = x;
		cout << "Zastapiono " << v << " przez " << x << "." << endl;
	}
}

/*
void Graf::replace_kraw(int v, int x)
{
	int i = 0;
	while (v != lista_krawedzi[i]->dane)
		i++;
	lista_krawedzi[i]->dane = x;
}
*/

int Graf::indeks_wezla(int cos)
{
	for (int i = 0; i < lista_wezlow.size(); i++) 
	{
		if (lista_wezlow[i]->dane == cos)
			return i; 
	}
	return -1; 
}

void Graf::powieksz()
{
	rozmiar = rozmiar * 2;
	Krawedz* **Temp = new Krawedz* *[rozmiar];
	for (int i = 0; i < rozmiar; i++) {
		Temp[i] = new Krawedz*[rozmiar];
		for (int j = 0; j < rozmiar; j++)
			Temp[i][j] = nullptr;
	}
	for (int i = 0; i < rozmiar / 2; i++) {
		for (int j = 0; j < rozmiar / 2; j++)
			Temp[i][j] = matrix[i][j];
	}
	matrix = Temp;
}

void Graf::insert_Node(int cos)
{
	if (indeks_wezla(cos) == -1) 
	{
		Node *wezel = new Node(cos);
		lista_wezlow.push_back(wezel);
		if (ilosc + 1 == rozmiar) 
		{
			powieksz();
		}
		wezel->indeks = ilosc;
		ilosc++;
		cout << "Dodano wierzcholek o wartosci " << cos << "." << endl;
	}
	else
		cout << "W grafie jest juz wierzcholek o podanej wartosci!" << endl;
}

void Graf::insert_Kraw(int cos, int pol, int lop)
{
	if (indeks_wezla(pol) == -1)
	{
		insert_Node(pol);
	}
	if (indeks_wezla(lop) == -1)
	{
		insert_Node(lop);
	}
	Krawedz *krawedz = new Krawedz(cos, lista_wezlow[indeks_wezla(pol)], lista_wezlow[indeks_wezla(lop)]); 
	matrix[lista_wezlow[indeks_wezla(pol)]->indeks][lista_wezlow[indeks_wezla(lop)]->indeks] = krawedz;
	matrix[lista_wezlow[indeks_wezla(lop)]->indeks][lista_wezlow[indeks_wezla(pol)]->indeks] = krawedz;
	cout << "Do wierzcholkow " << pol << " i " << lop << " dodano krawedz o wartosci " << cos << endl;
}

void Graf::usun_krawedz(int cos)
{
	int usunieto = 0; 
	if (lista_wezlow.size() == 0) { 
		cout << "Graf jest pusty!" << endl;
	}
	else
	{
		for (int i = 0; i < lista_wezlow.size(); i++)
		{ 
			for (int j = 0; j < lista_wezlow.size(); j++)
			{
				if (matrix[i][j] != nullptr) { 
					if (matrix[i][j]->dane == cos) 
					{
						matrix[i][j] = nullptr;
						usunieto++;
					}
				}
			}
		}
		if (usunieto == 0) 
			cout << "Podana krawedz nie istnieje!\n";
		else
			cout << "Krawedz o wartosci " << cos << " zostala usunieta\n";
	}
}

void Graf::usun(int cos)
{
	int indeks = indeks_wezla(cos);
	if (indeks == -1) 
		cout << "Niestety, nie ma akiego wierzcholka."<<endl;
	else 
	{
		for (int i = 0; i < lista_wezlow.size(); i++) 
		{
			if (matrix[indeks][i] != nullptr) 
				usun_krawedz(matrix[indeks][i]->dane);
		}
		for (int i = 0; i < lista_wezlow.size(); i++) 
		{
			for (int j = 0; j < lista_wezlow.size(); j++) 
			{
				if (i > indeks && j > indeks) 
				{
					matrix[i - 1][j - 1] = matrix[i][j];
					matrix[i][j] = nullptr;
				}
				else if (i > indeks) {
					matrix[i - 1][j] = matrix[i][j];
					matrix[i][j] = nullptr;
				}
				else if (j > indeks) {
					matrix[i][j - 1] = matrix[i][j];
					matrix[i][j] = nullptr;
				}
			}
		}
		lista_wezlow.erase(lista_wezlow.begin() + indeks); 
		for (int i = indeks; i < lista_wezlow.size(); i++)
			lista_wezlow[i]->indeks--; 
		ilosc--;
		cout << "Wierzcholek o wartosci " << cos << " zostal usuniety." << endl;
	}
}

void Graf::pokaz_swoje_krawedzie(int v)
{
	if (indeks_wezla(v) != -1)
	{
		int i = indeks_wezla(v);
		cout << "Krawedzie wierzcholka " << lista_wezlow[i]->dane << ": "<<endl;
		for (int j = 0; j < lista_wezlow.size(); j++)
		{
			if (matrix[i][j] != nullptr)
				cout << matrix[i][j]->dane << endl;
		}
		cout << endl;
	}
	else
	{
		cout << "Brak wybranego wezla." << endl;
	}
}

void Graf::pokaz_wszystkie_krawedzie()
{
	if (lista_wezlow.size() != 0)
	{
		cout << endl << "Wszystkie krawedzie w grafie:" << endl;
		for (int i = 0; i < lista_wezlow.size(); i++)
		{
			cout << "Krawedzie wierzcholka " << lista_wezlow[i]->dane << ": ";
			for (int j = 0; j < lista_wezlow.size(); j++)
			{
				if (matrix[i][j] != nullptr)
					cout << matrix[i][j]->dane << endl;
			}
			cout << endl;
		}
	}
	else
	{
		cout << "Brak wezlow w grafie." << endl;
	}
}

void Graf::pokaz_wszystkie_wezly()
{
	if (lista_wezlow.size() != 0)
	{
		cout << endl << "Wszystkie wierzcholki w grafie:" << endl;;
		for (int i = 0; i < lista_wezlow.size(); i++)
		{
			cout << lista_wezlow[i]->dane << endl;
		}
		cout << endl;
	}
	else
	{
		cout << "Graf jest pusty." << endl;
	}
}

int main()
{
	Graf *Grafik = new Graf;

	Grafik->insert_Kraw(6, 7, 8);
	Grafik->insert_Kraw(17, 7, 5);
	Grafik->insert_Kraw(18, 21, 22);
	Grafik->insert_Kraw(2, 3, 4);
	Grafik->insert_Kraw(5, 1, 2);
	Grafik->insert_Kraw(50, 7, 71);
	Grafik->insert_Kraw(900, 7, 212);
	Grafik->insert_Kraw(180, 20, 7);

	Node *cos = Grafik->opposite(7,6);
	Grafik->pokaz_swoje_krawedzie(7);
	Grafik->pokaz_wszystkie_wezly();
	Grafik->usun(7);
	Grafik->pokaz_wszystkie_wezly();
	return 0;
}