#include "stdafx.h"
#include "iostream"
#include <string>
#include <vector>

using namespace std;

void dodaj_lin(int rozmiar, int liczba, int *Tab)
{
	int reszta;
	int cos = 1;
	int pomoc;
	int licz = 0;
	pomoc = liczba;
	reszta = liczba%rozmiar;

	for (int i = 1; i < rozmiar + 1; i++)
	{
		if (cos == 1) {
			if (Tab[reszta] == -6)
			{
				Tab[reszta] = liczba;
				cos = 0;
				i = rozmiar + 2;
			}
			else
				reszta = (pomoc + i) % rozmiar;
		}
		licz = licz + 1;
	}
	if (cos == 1)
		cout << "Tablica jest pelna" << endl;
	cout << "Odwiedzono " << licz << " komorek." << endl;
}

void usun(int rozmiar, int liczba, int *Tab)
{
	int reszta;
	int cos = 1;
	int licz = 0;
	int pomoc;
	reszta = liczba%rozmiar;
	pomoc = reszta;

	for (int i = 1; i < rozmiar + 1; i++)
	{
		if (cos == 1) 
		{
			if (Tab[reszta] == liczba)
			{
				Tab[reszta] = -6;
				cos = 0;
				i = rozmiar + 2;
			}
			else
				reszta = (pomoc + i) % rozmiar;
		}
		licz = licz + 1;
	}
	if(cos == 0)
	cout << "Usunieto szukany element." << endl;
	if (cos == 1)
		cout << "Nie znalezionoe szukanego elementu." << endl;
	cout << "Odwiedzono " << licz << " komorek." << endl;
}

int wyszukaj(int rozmiar, int liczba, int *Tab)
{
	int reszta;
	int cos = 1;
	int licz = 0;
	int pomoc;
	reszta = liczba%rozmiar;
	pomoc = reszta;
	licz = reszta;

	for (int i = 1; i < rozmiar + 1; i++)
	{
		if (cos == 1) {
			if (Tab[reszta] == liczba)
			{
				cos = 0;
				i = rozmiar + 2;
				licz++;
			}
			else
			{
				reszta = (pomoc + i) % rozmiar;
				licz++;
			}
		}
	}
	if (cos == 1)
		licz = rozmiar + 2;
	return licz;
}

void wyswietl(int rozmiar, int *Tab)
{
	cout << "Oto twoja tablica: " << endl;
	for (int i = 0; i < rozmiar; i++)
	{
		if (Tab[i] != -6)
		{
			cout << Tab[i] << " ";
		}
	}
	cout << endl;
}

void dodaj_link(int rozmiar,int liczba, vector<int> *Tab)
{
	int reszta;
	reszta = liczba%rozmiar;
	Tab[reszta].push_back(liczba);
	cout << "Odwiedzono " << Tab->size()+1 << " komorek." << endl;
}

void wyswietl_link(int rozmiar, vector<int> *Tab)
{
	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < Tab[i].size(); j++)
			cout << Tab[i][j] << " ";
	}
	cout << endl;
}
	
void wyszukaj_link(int rozmiar, int liczba, vector<int> *Tab)
{
	int reszta;
	int cos;
	int licz = 0;
	reszta = liczba%rozmiar;
	for (int i = 0; i < Tab[reszta].size(); i++)
	{
		cos = Tab[reszta][i];
		if (cos == liczba)
		{
			licz++;
			i = Tab[reszta].size();
		}
		else
		{
		cout << "pomoc!!!" << endl;
			licz++;
		}
	}
	if (cos == liczba)
	{
		cout << "Znaleziono liczbe w " << licz << " elemencie " << reszta+1 << " komorki." << endl;
	}
	else
	{
		cout << "Brak szukanej liczby.";
	}
}

void usun_link(int rozmiar, int liczba, vector<int> *Tab)
{
	int reszta;
	int cos = 0;
	int licz = 0;
	reszta = liczba%rozmiar;
	for (int i = 0; i < Tab[reszta].size(); i++)
	{
		cos = Tab[reszta][i];
		if (cos == liczba)
		{
			//licz++;
			i = Tab[reszta].size();
		}
		else
		{
			//cout << "pomoc!!!" << endl;
			licz++;
		}
	}
	if (cos == liczba)
	{
		cout << "Odwiedzono " << licz +2<< " komorek." << endl;
		cout << "Pomyslnie usunieto szukana liczbe." << endl;
		Tab[reszta].erase(Tab[reszta].begin()+licz);
	}
	else
	{
		cout << "Odwiedzono " << licz +2<< " komorek." << endl;
		cout << "Brak szukanej liczby."<<endl;
	}
}

void dodaj_2x(int rozmiar, int liczba, int *Tab)
{
	int reszta = liczba % rozmiar;
	int pizza = liczba % 7;
	int licz = 0;
	int cos = 0;

	for (int i = 0; i < rozmiar; i++) {
		if (Tab[reszta] == -6)
		{
			licz++;
			cos = 1;
			i = rozmiar;
			Tab[reszta] = liczba;
		}
		else
		{
			licz++;
			reszta = reszta + pizza;
			if (reszta >= rozmiar)
			{
				reszta = reszta - rozmiar;
			}
		}
	}
	cout << "Odwiedzono " << licz << " komorek." << endl;
	if (cos == 0)
		cout << "Tablica jest pelna." << endl;
}

void wyszukaj_2x(int rozmiar, int liczba, int *Tab)
{
	int reszta = liczba % rozmiar;
	int pizza = liczba % 7;
	int licz = 0;
	int cos = 0;

	for (int i = 0; i < rozmiar; i++) {
		if (Tab[reszta] == liczba)
		{
			licz++;
			cos = 1;
			i = rozmiar;
			//Tab[reszta] = liczba;
		}
		else
		{
			licz++;
			reszta = reszta + pizza;
			if (reszta >= rozmiar)
			{
				reszta = reszta - rozmiar;
			}
		}
	}
	if (cos == 0)
		cout << "Brak takiego elementu." << endl;
	else
		cout << "Znaleziono poszukiwany element w " << licz << " komorce tablicy." << endl;
}

void usun_2x(int rozmiar, int liczba, int *Tab)
{
	int reszta = liczba % rozmiar;
	int pizza = liczba % 7;
	int licz = 0;
	int cos = 0;

	for (int i = 0; i < rozmiar; i++) {
		if (Tab[reszta] == liczba)
		{
			licz++;
			cos = 1;
			i = rozmiar;
			Tab[reszta] = -6;
		}
		else
		{
			licz++;
			reszta = reszta + pizza;
			if (reszta >= rozmiar)
			{
				reszta = reszta - rozmiar;
			}
		}
	}
	if (cos == 0)
		cout << "Brak takiego elementu." << endl;
	else
		cout << "Element usunieto pomyslnie."<< endl;
	cout << "Odwiedzono " << licz << " komorek." << endl;
}

int main()
{
	//int zwrot;
	int rozmiar;
	int c = 0;
	int i = 0;
	int k[4] = { 2 ,3,5,7 };
	int liczba;
	while (i != 4)
	{
		cout << "Podaj rozmiar tablicy: ";
		cin >> rozmiar;
		cout << endl;
		while (rozmiar%k[i] != 0 && i != 4)
		{
			i++;
		}
		if (i != 4)
		{
			cout << "Podaj liczbe pierwsza." << endl;
		}
	}
	int *Tab = new int[rozmiar];
	for (int i = 0; i < rozmiar; i++)
	{
		Tab[i] = -6;
	}
	int *help = Tab;
	vector<int> *Tab_v = new vector<int>[rozmiar];
	
	/*
	do {
		cout << "Wybierz jedna z ponizszych opcji: " << endl;
		cout << "1. Wprowadz nowa liczbe do tablicy." << endl;
		cout << "2. Usun wybrany element." << endl;
		cout << "3. Wyszukaj wybrany element." << endl;
		cout << "4. Wyswietl tablice." << endl;
		cout << "0. Zakoncz." << endl;
		cin >> c;
		cout << endl;
		switch (c) {
		case 1: {
			cout << "Podaj liczbe: ";
			cin >> liczba;
			cout << endl;
			dodaj_lin(rozmiar, liczba, Tab);
		};
				  break;
		
		case 2: {
			cout << "Podaj liczbe: ";
			cin >> liczba;
			cout << endl;
			usun(rozmiar, liczba, Tab);
		};
				break;

		case 3: {
			cout << "Podaj poszukiwana liczbe: ";
			cin >> liczba;
			cout << endl;
			zwrot = wyszukaj(rozmiar, liczba, Tab);
			if (zwrot == rozmiar + 2)
				cout << "Brak szukanego elementu w tablicy." << endl;
			else
				cout << "Szukany element znajduje sie na " << zwrot << " miejscu tablicy." << endl;
		}

		case 4: {
			wyswietl(rozmiar, Tab);
		}
		}
	} while (c != 0);
	
	for (int i = 0; i < rozmiar; i++)
	{
		liczba = rand() % 101;
		dodaj_lin(rozmiar, liczba, Tab);
	}
	wyswietl(rozmiar, Tab);
	*/
	/*
	for (int i = 0; i < rozmiar; i++)
	{
		liczba = rand() % 101;
		dodaj_link(rozmiar, liczba, Tab_v);
	}
	dodaj_link(rozmiar, 69, Tab_v);
	wyswietl_link(rozmiar, Tab_v);
	usun_link(rozmiar, 102, Tab_v);
	wyswietl_link(rozmiar, Tab_v);
	*/
	cout << endl;
	cout << "Podwojne.\n" << endl;
	for (int i = 0; i < rozmiar+4; i++)
	{
		liczba = rand() % 101;
		cout << "Dodano element " << liczba << "." << endl;
		dodaj_2x(rozmiar, liczba, Tab);
	}
	wyswietl(rozmiar, Tab);
	usun_2x(rozmiar, 71, Tab);
	wyswietl(rozmiar, Tab);
	//int *Tab = new int[rozmiar];
	for (int i = 0; i < rozmiar; i++)
	{
		Tab[i] = -6;
	}

	cout << endl;
	cout << "Linkowanie.\n" << endl;
	for (int i = 0; i < rozmiar +4; i++)
	{
		liczba = rand() % 101;
		cout << "Dodano element " << liczba << "." << endl;
		dodaj_link(rozmiar, liczba, Tab_v);
	}
	wyswietl_link(rozmiar, Tab_v);
	usun_link(rozmiar, 71, Tab_v);
	wyswietl_link(rozmiar, Tab_v);

	cout << endl;
	cout << "Liniowe.\n" << endl;
	//int *Tab = new int[rozmiar];
	for (int i = 0; i < rozmiar; i++)
	{
		Tab[i] = -6;
	}
	for (int i = 0; i < rozmiar+4; i++)
	{
		liczba = rand() % 101;
		cout << "Dodano element " << liczba << "." << endl;
		dodaj_lin(rozmiar, liczba, Tab);
	}
	wyswietl(rozmiar, Tab);
	usun(rozmiar, 71, Tab);
	wyswietl(rozmiar, Tab);
	return 0;
}

