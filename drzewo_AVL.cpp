#include "stdafx.h"
#include <cstdio>
#include <iostream>
#include <string>
#include <list>
using namespace std;

//int h = 0;
template<typename pizza>
class Node {
public:
	int rozmiar;
	int position;
	int size;
	int LczyP;
	pizza information;
	Node *LSyn;
	Node *PSyn;
	Node *vater;
	//Node **sohne;
	Node<pizza> get_vater();
	bool is_vater();
	void set_vater(Node<pizza> *vater);
	pizza get_info();
	void set_info(pizza information);
	void add_sohn(Node<pizza> *sohn);
	void przesun();
	Node();
	Node(Node<pizza> *vater, pizza information);
};

template<typename pizza>
Node<pizza>::Node() {
	//sohne = new Node<pizza>[lop];
	//sohne = new Node*[2];
	LSyn = NULL;
	PSyn = NULL;
	size = 2;
	rozmiar = 0;
	vater = NULL;
	position = 0;
	//sohne = new Node<pizza>[rozmiar];
	//temp = NULL;
	//cout << "Node" << endl;
	//cout << "Node" << endl;
}

template<typename pizza>
Node<pizza>::Node(Node<pizza> *ojciec, pizza info)
{
	//sohne = new Node<pizza>[lop];
	//sohne = new Node*[2];
	LSyn = NULL;
	PSyn = NULL;
	size = 2;
	rozmiar = 2;
	position = 0;
	vater = ojciec;
	information = info;
}

template<typename pizza>
Node<pizza> Node<pizza>::get_vater() {
	return *vater;
}

template<typename pizza>
bool Node<pizza>::is_vater() {
	//cout << "isvater" << endl;
	if (vater == NULL)
	{
		return false;
	}
	else {
		return true;
	}
}

template<typename pizza>
void Node<pizza>::set_vater(Node<pizza> *ojciec) {
	vater = ojciec;
}

template<typename pizza>
void Node<pizza>::przesun() {
	position = position + 1;
}

template<typename pizza>
pizza Node<pizza>::get_info() {
	return information;
}

template<typename pizza>
void Node<pizza>::set_info(pizza info) {
	information = info;
}

template<typename pizza>
void Node<pizza>::add_sohn(Node<pizza> *cos) {
	Node<pizza> * temp = new Node<pizza>();
	temp = this;
	while (temp != nullptr)
	{
		if (cos->information < temp->information)
		{
			temp = temp->LSyn;
		}
		else
		{
			temp = temp->PSyn;
		}
	}
	temp = cos;
}

template<typename pizza>
class Baum {
public:
	Node<pizza> *root;
	void Pre_order(Node<pizza> *wezel);
	void Post_order(Node<pizza> *wezel);
	void In_order(Node<pizza> *wezel);
	Node<pizza> *korzen();
	Node<pizza> *max(Node<pizza> *wezel);
	void usun(Node<pizza> *wezel);
	void usun(pizza dane, Node<pizza> *wezel);
	//void wysokosc(int licznik, Node<pizza> *wezel);
	void dodaj_wezel(pizza dane);
	void dodaj_wezel(pizza dane, Node<pizza> *wezel);
	//Node(Node<pizza> *vater, pizza information);
	Node<pizza> *rr_rotation(Node<pizza> *vater);
	Node<pizza> *ll_rotation(Node<pizza> *vater);
	Node<pizza> *lr_rotation(Node<pizza> *vater);
	Node<pizza> *rl_rotation(Node<pizza> *vater);
	Node<pizza> *balance(Baum<pizza> *drzewo, Node<pizza> *temp);
	Baum();
	Baum(Node<pizza> *root);
};

template<typename pizza>
Baum<pizza>::Baum() {
	root = NULL;
}

template<typename pizza>
Node<pizza> *Baum<pizza>::korzen() {
	Node<pizza> *temp = new Node<pizza>();
	temp = root;
	return temp;
}

template<typename pizza>
Node<pizza>* Baum<pizza>::max(Node<pizza> *wezel)
{
	Node<pizza>* temp = new Node<pizza>();
	temp = wezel;
	while (temp->PSyn != nullptr)
	{
		temp = temp->PSyn;
	}
	return temp;
}

template<typename pizza>
pizza porownaj(pizza stara, pizza wprowadzana)
{
	if (stara > wprowadzana)
		return stara;
	else
		return wprowadzana;
}

template<typename pizza>
int wysokosc_prawego(Baum<pizza> *drzewo, Node<pizza> *wezel)
{
	int h = 0;
	if (wezel == drzewo->root) {
		h = porownaj(h, wysokosc(drzewo, wezel->PSyn));
		return h + 1;
	}
	else {
		if (wezel->PSyn == nullptr && wezel->LSyn == nullptr)
			return 0;
		else
		{
			if (wezel->LSyn != nullptr)
			{
				//int h = 0;
				h = porownaj(h, wysokosc(drzewo, wezel->LSyn));
			}
			if (wezel->PSyn != nullptr)
			{
				//int h = 0;
				h = porownaj(h, wysokosc(drzewo, wezel->PSyn));
			}
			return h + 1;
		}
	}
}

template<typename pizza>
int wysokosc_lewego(Baum<pizza> *drzewo, Node<pizza> *wezel)
{
	int h = 0;
	if (wezel == drzewo->root) {
		h = porownaj(h, wysokosc(drzewo, wezel->LSyn));
		return h + 1;
	}
	else{
		if (wezel->PSyn == nullptr && wezel->LSyn == nullptr)
			return 0;
		else
		{
			if (wezel->LSyn != nullptr)
			{
				//int h = 0;
				h = porownaj(h, wysokosc(drzewo, wezel->LSyn));
			}
			if (wezel->PSyn != nullptr)
			{
				//int h = 0;
				h = porownaj(h, wysokosc(drzewo, wezel->PSyn));
			}
			return h + 1;
		}
	}
}

template<typename pizza>
int wysokosc(Baum<pizza> *drzewo, Node<pizza> *wezel)
{
	int h = 0;
	if (wezel->PSyn == nullptr && wezel->LSyn == nullptr)
		return 0;
	else
	{
		if (wezel->LSyn != nullptr)
		{
			//int h = 0;
			h = porownaj(h, wysokosc(drzewo, wezel->LSyn));
		}
		if (wezel->PSyn != nullptr)
		{
			//int h = 0;
			h = porownaj(h, wysokosc(drzewo, wezel->PSyn));
		}
		return h + 1;
	}
}

template<typename pizza>
int roznica(Baum<pizza> *drzewo, Node<pizza> *wezel)
{
	int cos = wysokosc_lewego(drzewo, wezel) - wysokosc_prawego(drzewo, wezel);
	//cos = sqrt(cos*cos);
	return cos;
}

template<typename pizza>
bool czybalans(Baum<pizza> *drzewo, Node<pizza> *wezel)
{
	int cos = roznica(drzewo, wezel);
	cos = sqrt(cos*cos);
	if (cos > 1)
	{
		cout << "Brak balansu." << endl;
		return false;
	}
	else
	{
		cout << "Jest balans." << endl;
		return true;
	}
}

template<typename pizza>
Baum<pizza>::Baum(Node<pizza> *nowy)
{
	root = nowy;
}

template<typename pizza>
void Baum<pizza>::Pre_order(Node<pizza> *wezel)
{
	cout << wezel->information << " ";
	if (wezel->LSyn != nullptr)
		Pre_order(wezel->LSyn);
	if (wezel->PSyn != nullptr)
		Pre_order(wezel->PSyn);
}

template<typename pizza>
void Baum<pizza>::Post_order(Node<pizza> *wezel)
{
	if (wezel->LSyn != nullptr)
		Post_order(wezel->LSyn);
	if (wezel->PSyn != nullptr)
		Post_order(wezel->PSyn);
	cout << wezel->information << " ";
}

template<typename pizza>
void Baum<pizza>::In_order(Node<pizza> *wezel)
{
	if (wezel->LSyn != nullptr)
		In_order(wezel->LSyn);
	cout << wezel->information << " ";
	if (wezel->PSyn != nullptr)
		In_order(wezel->PSyn);
}

template<typename pizza>
void Baum<pizza>::dodaj_wezel(pizza information, Node<pizza> *lisc)
{
	if (information < lisc->information) {
		if (lisc->LSyn != nullptr)
		{
			dodaj_wezel(information, lisc->LSyn);
		}
		else
		{
			Node<pizza> *nowy = new Node<pizza>;
			nowy->information = information;
			nowy->LczyP = 0;
			lisc->LSyn = nowy;
			lisc->LSyn->vater = lisc;
			lisc->LSyn->LSyn = nullptr;
			lisc->LSyn->PSyn = nullptr;
		}
	}
	else if (information >= lisc->information) {
		if (lisc->PSyn != nullptr)
		{
			dodaj_wezel(information, lisc->PSyn);
		}
		else
		{
			Node<pizza> *nowy = new Node<pizza>;
			nowy->information = information;
			nowy->LczyP = 1;
			lisc->PSyn = nowy;
			lisc->PSyn->vater = lisc;
			lisc->PSyn->LSyn = nullptr;
			lisc->PSyn->PSyn = nullptr;
		}
	}
}

template<typename pizza>
void Baum<pizza>::dodaj_wezel(pizza dane)
{
	if (root != nullptr)
		dodaj_wezel(dane, root);
	else {
		Node<pizza> *nowy = new Node<pizza>;
		nowy->information = dane;
		root = nowy;
		root->LSyn = nullptr;
		root->PSyn = nullptr;
	}
}

template<typename pizza>
void Baum<pizza>::usun(pizza usuwam, Node<pizza> *wezel)
{
	int	rozmiar = wezel->rozmiar;
	Node<pizza> *temp = new Node<pizza>();
	Node<pizza> *halp = new Node<pizza>();
	//for (int i = 0; i < rozmiar; i++)
	if (usuwam < wezel->information)
	{
		//cout << "element L: " << wezel->LSyn->information << endl;
		if (usuwam == wezel->information)
		{
			if (wezel->LSyn == nullptr && wezel->PSyn == nullptr)
			{
				wezel = nullptr;
			}
			else
			{
				if (wezel->PSyn != nullptr)
				{
					temp = wezel->PSyn;
					while (temp->LSyn != nullptr)
					{
						temp = temp->LSyn;
					}
					wezel->information = temp->information;
					if (temp->PSyn != nullptr)
					{
						halp = temp->PSyn;
						if (temp->vater->LSyn->PSyn == halp)
							temp->vater->LSyn = halp;
						else
							temp->vater->PSyn = halp;
					}
					else
						temp->vater->LSyn = nullptr;
				}
				else
				{
					temp = wezel->LSyn;
					while (temp->PSyn != nullptr)
					{
						temp = temp->PSyn;
					}
					wezel->information = temp->information;
					if (temp->PSyn != nullptr)
					{
						halp = temp->PSyn;
						if (temp->vater->LSyn->PSyn == halp)
							temp->vater->LSyn = halp;
						else
							temp->vater->PSyn = halp;
					}
					else
						temp->vater->LSyn = nullptr;
				}
			}
		}
		else
		{
			temp = wezel->LSyn;
			usun(usuwam, temp);
		}
	}
	else
	{
		//cout << "element P: " << wezel->PSyn->information << endl;
		if (usuwam == wezel->information)
		{
			if (wezel->LSyn == nullptr && wezel->PSyn == nullptr)
			{
				if (wezel->vater->PSyn == wezel)
					wezel->vater->PSyn = nullptr;
				else
					wezel->vater->LSyn = nullptr;
			}
			else
			{
				if (wezel->PSyn != nullptr)
				{
					temp = wezel->PSyn;
					while (temp->LSyn != nullptr)
					{
						temp = temp->LSyn;
					}
					wezel->information = temp->information;
					if (temp->PSyn != nullptr)
					{
						halp = temp->PSyn;
						if (temp->vater->LSyn != nullptr) {
							if (temp->vater->LSyn->PSyn == halp)
								temp->vater->LSyn = halp;
						}
						if (temp->vater->PSyn != nullptr) {
							temp->vater->PSyn = halp;
						}
					}
					else {
						if (temp->vater->LSyn != nullptr) {
							if (temp->vater->LSyn->information == wezel->information)
								temp->vater->LSyn = nullptr;
						}
						else
							temp->vater->PSyn = nullptr;
					}
				}
				else
				{
					temp = wezel->LSyn;
					while (temp->PSyn != nullptr)
					{
						temp = temp->PSyn;
					}
					wezel->information = temp->information;
					if (temp->LSyn != nullptr)
					{
						halp = temp->LSyn;
						if (temp->vater->LSyn != nullptr) {
							if (temp->vater->PSyn->LSyn->information == halp->information)
								temp->vater->PSyn = halp;
						}
						else
							temp->vater->LSyn = halp;
					}
					else
					{
						if (temp->vater->LSyn != nullptr) {
							if (temp->vater->LSyn->information == wezel->information)
								temp->vater->LSyn = nullptr;
						}
						if (temp->vater->PSyn != nullptr)
							temp->vater->PSyn = nullptr;
					}
				}
			}
		}
		else
		{
			temp = wezel->PSyn;
			usun(usuwam, temp);
		}
	}
}

template<typename pizza>
void Baum<pizza>::usun(Node<pizza> *wezel)
{
	//cout << "wchodze" << endl;
	Node<pizza> *temp = new Node<pizza>;
	int j = 0;
	if (wezel != nullptr && wezel->vater != nullptr)
	{
		temp = wezel->vater;
		if (temp->LSyn == wezel)
			temp->LSyn = NULL;
		else
			temp->PSyn = NULL;
	}
	else
		cout << "brak takiego wezla" << endl;
	//temp->sohne = nowa;
	//temp->rozmiar = rozmiar - 1;
}

template <typename pizza>
Node<pizza> *Baum<pizza>::rr_rotation(Node<pizza> *vater)
{
	Node<pizza> *temp;
	temp = vater->PSyn;
	vater->PSyn = temp->LSyn;
	temp->LSyn = vater;
	return temp;
}

template <typename pizza>
Node<pizza> *Baum<pizza>::ll_rotation(Node<pizza> *vater)
{
	Node<pizza> *temp;
	temp = vater->LSyn;
	vater->LSyn = temp->PSyn;
	temp->PSyn = vater;
	return temp;
}

template <typename pizza>
Node<pizza> *Baum<pizza>::lr_rotation(Node<pizza> *vater)
{
	Node<pizza> *temp;
	temp = vater->LSyn;
	vater->LSyn = rr_rotation(temp);
	return ll_rotation(vater);
}

template <typename pizza>
Node<pizza> *Baum<pizza>::rl_rotation(Node<pizza> *vater)
{
	Node<pizza> *temp;
	temp = vater->PSyn;
	vater->PSyn = ll_rotation(temp);
	return rr_rotation(vater);
}

template <typename pizza>
Node<pizza> *Baum<pizza>::balance(Baum<pizza> *drzewo, Node<pizza> *temp)
{
	int bal = roznica(drzewo, temp);
	if (bal > 1)
	{
		if (roznica(drzewo,temp->LSyn) > 0)
			temp = ll_rotation(temp);
		else
			temp = lr_rotation(temp);
	}
	else if (bal < -1)
	{
		if (roznica(drzewo,temp->PSyn) > 0)
			temp = rl_rotation(temp);
		else
			temp = rr_rotation(temp);
	}
	return temp;
}


int main()
{
	Node<int> *wezel = new Node<int>(NULL, 3);
	Node<int> *temp = new Node<int>();
	int dane;
	//dane = "cos 2";
	Baum<int> *drzewo = new Baum<int>(wezel);
	//cout << "1"<<endl;
	dane = 1;
	drzewo->dodaj_wezel(dane);
	//cout << "2" << endl;
	dane = 2;
	drzewo->dodaj_wezel(dane);
	dane = 7;
	drzewo->dodaj_wezel(dane);
	dane = 8;
	drzewo->dodaj_wezel(dane);
	dane = 12;
	drzewo->dodaj_wezel(dane);
	dane = 19;
	drzewo->dodaj_wezel(dane);
	dane = 9;
	drzewo->dodaj_wezel(dane);
	dane = 6;
	drzewo->dodaj_wezel(dane);
	dane = 3;
	drzewo->dodaj_wezel(dane);
	//cout << "1" << endl;
	//cout << drzewo->wysokosc(0, wezel) << endl;
	//h = 0;
	cout << "WYSOKOSC: ";
	cout << wysokosc(drzewo, wezel) << endl;
	cout << "WYSOKOSC L: ";
	cout << wysokosc_lewego(drzewo, wezel) << endl;
	cout << "WYSOKOSC P: ";
	cout << wysokosc_prawego(drzewo, wezel) << endl;
	czybalans(drzewo, wezel);
	cout << "Pre" << endl;
	drzewo->Pre_order(wezel);
	cout << endl;
	cout << "Post" << endl;
	drzewo->Post_order(wezel);
	cout << endl;
	cout << "In" << endl;
	drzewo->In_order(wezel);
	cout << endl;
	temp = wezel;
	temp = temp->LSyn;
	//drzewo->usun(temp);
	cout << "USUWAM" << endl;
	//drzewo->usun(7, wezel);
	cout << "WYSOKOSC: ";
	cout << wysokosc(drzewo, wezel) << endl;
	cout << "WYSOKOSC L: ";
	cout << wysokosc_lewego(drzewo, wezel) << endl;
	cout << "WYSOKOSC P: ";
	cout << wysokosc_prawego(drzewo, wezel) << endl;
	czybalans(drzewo, wezel);
	wezel = drzewo->balance(drzewo, wezel);
	cout << "WYSOKOSC L: ";
	cout << wysokosc_lewego(drzewo, wezel) << endl;
	cout << "WYSOKOSC P: ";
	cout << wysokosc_prawego(drzewo, wezel) << endl;
	czybalans(drzewo, wezel);
	cout << "Pre" << endl;
	drzewo->Pre_order(wezel);
	cout << endl;
	//cout << "3" << endl;
	//cout << h << endl;
	//h = 0;
	cout << "Post" << endl;
	drzewo->Post_order(wezel);
	cout << endl;
	cout << "In" << endl;
	drzewo->In_order(wezel);
	cout << endl;
	//cout <<  temp->get_info()<< endl;;
	return 0;
}